/*******************************************************************************
 * apps_launcher.cpp
 *
 * History:
 *   2014-9-28 - [lysun] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#include "am_base_include.h"
#include "am_log.h"
#include "am_define.h"
#include "am_file.h"
#include <fstream>
#include <signal.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/mount.h>
#include "am_pid_lock.h"
#include "commands/am_service_impl.h"
#include "am_api_proxy.h"
#include "am_service_manager.h"
#include "am_configure.h"
#include "apps_launcher.h"
#include "am_watchdog.h"
#include "am_config_set.h"

#define DEFAULT_APPS_CONF_FILE  "/etc/oryx/apps/apps_launcher.acs"
#define FIRST_RUN "/etc/oryx/apps/first_run"
#define PROC_MTD  "/proc/mtd"
#define PROC_MOUNTS_FILE  "/proc/mounts"
#define MTDBLOCK_DEVICE_PATH  "/dev/mtdblock"
#define DEFAULT_MOUNT_FS_TYPE "ubifs"
#define PARTITION_NAME  "add"
#define MOUNT_DIR "/dev/add"
#define ORYX_PATH "/etc/oryx"
#define ORYX_BASE "oryx_base.tar.xz"
#define PATH_LEN 128

using namespace std;

static AMWatchdogService* gWdInstance = NULL;

static void sigstop(int arg)
{
  INFO("signal caught\n");
}



static AMAppsLauncher *m_instance = NULL;

AMAppsLauncher::AMAppsLauncher():
        m_service_manager(NULL),
        m_api_proxy(NULL)
{
  memset(m_service_list, 0, sizeof(m_service_list));
}

AMAppsLauncher *AMAppsLauncher::get_instance()
{
  if (!m_instance) {
    m_instance = new AMAppsLauncher();
  }
  return m_instance;
}

AMAppsLauncher::~AMAppsLauncher()
{
  m_instance = NULL;
  delete m_api_proxy;
}


int AMAppsLauncher::load_config()
{

  return 0;
}

void AMAppsLauncher::get_ubi_node(const string &index_str, string &ubi_node)
{
  char file_path[PATH_LEN] = {0};
  string line_str;
  ifstream in_file;
  DIR *dirp;
  struct dirent *direntp;



  do {
    ubi_node.clear();

    if ((dirp = opendir("/sys/class/ubi/")) == NULL) {
      ERROR("Failed to open folder /sys/class/ubi/.");
      break;
    }

    while ((direntp = readdir(dirp)) != NULL) {
      /* the folder name is like ubi1_0 after ubimkvol, so just check "_" */
      if ( !strncmp(direntp->d_name, "ubi", 3) && !strchr(direntp->d_name, '_')) {
        snprintf(file_path, PATH_LEN - 1,"/sys/class/ubi/%s/mtd_num", direntp->d_name);
        in_file.open(file_path, ios::in);
        if (!in_file.is_open()) {
          ERROR("Failed to open %s", file_path);
          break;
        }

        if (!in_file.eof()) {
          getline(in_file, line_str);
          if (line_str == index_str) {
            ubi_node = direntp->d_name;
            in_file.close();
            break;
          }
        }
        in_file.close();
      }
    }
    closedir(dirp);
  } while (0);
}

bool AMAppsLauncher::get_partition_index_str(const string &partition_name, string &index_str)
{
  bool ret = true;
  string line_str;
  string p_name;
  ifstream in_file;

  INFO("AMFWUpgrade::get_partition_index_str() is called.");
  do {
    if (partition_name.empty()) {
      NOTICE("partition_name is NULL");
      ret = false;
      break;
    }

    in_file.open(PROC_MTD, ios::in);
    if (!in_file.is_open()) {
      NOTICE("Failed to open %s", PROC_MTD);
      ret = false;
      break;
    }

    p_name = "\"" + partition_name + "\"";

    while (!in_file.eof()) {
      getline(in_file, line_str);
      if (line_str.find(p_name) != string::npos) {
        uint32_t s_pos;
        uint32_t e_pos;

        s_pos = line_str.find("mtd");
        if (s_pos == string::npos) {
          NOTICE("Can not find \"mtd\" in %s", line_str.c_str());
          ret = false;
          break;
        }

        e_pos = line_str.find_first_of(':', s_pos);
        if (e_pos == string::npos) {
          NOTICE("Can not find \":\" in %s", line_str.c_str());
          ret = false;
          break;
        }

        s_pos += 3;
        index_str = line_str.substr(s_pos, e_pos - s_pos);
        if (index_str.empty()) {
          NOTICE("Not find mtd index of %s", partition_name.c_str());
          ret = false;
          break;
        }
        break;
      }
    }

    in_file.close();

  } while (0);

  return ret;
}


bool AMAppsLauncher::is_mount(const string &mount_dst)
{
  bool ret = false;
  bool last_is_slash = false;
  int32_t len = 0;
  ifstream in_file;
  string mount_dir(mount_dst);
  string line_str;
  string::iterator it;

  for (it = mount_dir.begin(); it != mount_dir.end(); ++it) {
    if (*it == '/') {
      if (last_is_slash) {
        mount_dir.erase(it);
      } else {
        last_is_slash = true;
      }
    } else {
      last_is_slash = false;
    }
  }

  len = mount_dir.length();
  if (len > 1 && mount_dir[len - 1] == '/') {
    mount_dir = mount_dir.substr(0, len - 1);
  }

  in_file.open(PROC_MOUNTS_FILE, ios::in);
  if (!in_file.is_open()) {
    NOTICE("Failed to open %s", PROC_MOUNTS_FILE);
    ret = true;
  } else {
    while (!in_file.eof()) {
      getline(in_file, line_str);
      if (line_str.find(mount_dir) != string::npos) {
        ret = true;
        break;
      }
    }
    in_file.close();
  }

  return ret;
}

bool AMAppsLauncher::umount_dir(const string &mount_dst)
{
  bool ret = true;

  if (umount(mount_dst.c_str())) {
    PRINTF("Failed to umount %s\n", mount_dst.c_str());
    ret = false;
  }

  return ret;
}

bool AMAppsLauncher::mount_partition(const string &device_name,
                                     const string &mount_dst,
                                     const string &fs_type)
{
  string index_str("");
  string fs_type_str;
  string device_path(MTDBLOCK_DEVICE_PATH);
  bool ret = false;
  bool mount_flag = false;

  INFO("AMFWUpgrade::m_mount_partition() is called.");
  do {
    if (mount_dst.empty()) {
      NOTICE("mount_dst is \"\".");
      break;
    }

    if (fs_type.empty()) {
      fs_type_str = DEFAULT_MOUNT_FS_TYPE;
    } else {
      fs_type_str = fs_type;
    }

    if (!get_partition_index_str(device_name, index_str)) {
      break;
    }

    if (index_str.empty()) {
      PRINTF("Device partition %s not found.", device_name.c_str());
      break;
    }

    device_path += index_str;

    if (access(mount_dst.c_str(), F_OK)) {
      if (!AMFile::create_path(mount_dst.c_str())) {
        PRINTF("Failed to create dir %s\n", mount_dst.c_str());
        break;
      }
    } else {
      mount_flag = is_mount(mount_dst);
      if (mount_flag) {
        if (umount(mount_dst.c_str())) {
          PRINTF("Failed to umount %s\n", mount_dst.c_str());
          break;
        }
      }
    }

    if (fs_type_str == "ubifs") {
      int32_t status = 0;
      string cmd_line("");
      string ubi_nod("");
      string ubi_nod_path("");

      if (access("/dev/ubi_ctrl", F_OK)) {
        ERROR("mount ubifs: /dev/ubi_ctrl not found!");
        break;
      }

      get_ubi_node(index_str, ubi_nod);
      if (ubi_nod.empty()) {
        cmd_line = cmd_line + "ubiattach /dev/ubi_ctrl -m " + index_str;
        status = system(cmd_line.c_str());
        if (WEXITSTATUS(status)) {
          ERROR("%s :failed\n", cmd_line.c_str());
          break;
        }

        get_ubi_node(index_str, ubi_nod);
        if (ubi_nod.empty()) {
          ERROR("failed to ubiattach to %s partition", device_name.c_str());
          break;
        }
      }

      device_path.clear();
      device_path = "/dev/"+ ubi_nod + "_0";
      if (access(device_path.c_str(), F_OK)) {
        ubi_nod_path = "/dev/"+ ubi_nod;
        cmd_line.clear();
        cmd_line = "ubimkvol "+ ubi_nod_path + " -N " + device_name + " -m";
        status = system(cmd_line.c_str());
        if (WEXITSTATUS(status)) {
          ERROR("%s :failed\n", cmd_line.c_str());
          break;
        }
      }
    }

    if (mount(device_path.c_str(), mount_dst.c_str(),
              fs_type_str.c_str(), 0, 0)) {
      PRINTF("Failed mount %s to %s, mount fs_type: %s.",
             device_path.c_str(), mount_dst.c_str(), fs_type_str.c_str());
      PERROR("Mount failed.");
      break;
    } else {
      ret = true;
    }
  }while (0);

  return ret;
}

bool AMAppsLauncher::check_partition_exist(string partition_name)
{
  bool ret = false;
  std::ifstream in_file;
  std::string line_str;

  do {
    if (partition_name.empty()) {
      NOTICE("partition_name is NULL");
      break;
    }

    in_file.open(PROC_MTD, std::ios::in);
    if (!in_file.is_open()) {
      NOTICE("Failed to open %s", PROC_MTD);
      break;
    }

    while (!in_file.eof()) {
      std::getline(in_file, line_str);
      if (line_str.rfind(partition_name) != std::string::npos) {
        ret = true;
        break;
      }
    }

    in_file.close();

  } while (0);

  return ret;
}

bool AMAppsLauncher::touch_file(const string filename)
{
  bool ret = true;
  ofstream out_file;

  out_file.open(filename, ios::out);
  if (!out_file.is_open()) {
    NOTICE("Failed to open %s", filename.c_str());
    ret = false;
  } else {
    out_file.close();
  }

  return ret;
}

bool AMAppsLauncher::restore_config(const std::string &config_name,
                                    const std::string &config_path,
                                    const std::string &extract_path)
{
  bool ret = true;
  AMConfigSet *cfg_set = NULL;

  do {
    cfg_set = AMConfigSet::create(config_path);
    if (!cfg_set) {
      ERROR("Failed to create AMConfigSet instance.");
      ret = false;
      break;
    }

    if (!cfg_set->extract_config(config_name, extract_path)) {
      ERROR("Failed to extract %s to %s.",
            config_name.c_str(), extract_path.c_str());
      ret = false;
      break;
    }

  } while (0);

  delete cfg_set;
  cfg_set = NULL;

  return ret;
}

bool AMAppsLauncher::backup_config(const std::string &config_name,
                                   const std::string &config_list,
                                   const std::string &path_base,
                                   const std::string &store_path)
{
  bool ret = true;
  AMConfigSet *cfg_set = NULL;

  do {
    cfg_set = AMConfigSet::create(store_path);
    if (!cfg_set) {
      ERROR("Failed to create AMConfigSet instance.");
      ret = false;
      break;
    }

    if (!cfg_set->create_config(config_name, config_list, path_base)) {
      ERROR("Failed to create %s.", config_name);
      ret = false;
      break;
    }
  } while (0);

  delete cfg_set;
  cfg_set = NULL;

  return ret;
}

bool AMAppsLauncher::check_config(const string &config_path)
{
  bool ret = true;
  DIR *dirp;
  struct dirent *direntp;
  struct stat buf;
  string t_path;
  string c_path;
  vector<string> dir_list;
  int32_t len = 0;

  do {
    if (config_path.empty()) break;

    if (stat(config_path.c_str(), &buf) < 0) {
      PRINTF("stat %s error.\n", config_path.c_str());
      ret = false;
      break;
    } else {
      if (S_ISDIR(buf.st_mode)) {
        dir_list.push_back(config_path);
      } else {
        ERROR("%s is not a folder.\n", config_path.c_str());
        break;
      }
    }

    while (!dir_list.empty()) {
      c_path = dir_list.back();
      dir_list.pop_back();
      INFO("folder path=%s\n", c_path.c_str());
      if ((dirp = opendir(c_path.c_str())) == NULL) {
        ERROR("Failed to open folder %s.", c_path.c_str());
        ret = false;
        break;
      }
      while ((direntp = readdir(dirp)) != NULL) {
        /* the folder name is like ubi1_0 after ubimkvol, so just check "_" */
        if (strcmp(direntp->d_name, ".") && strcmp(direntp->d_name, "..")){
          t_path = c_path + "/" + direntp->d_name;
          INFO("file/path=%s\n", t_path.c_str());
          if (stat(t_path.c_str(), &buf) < 0) {
            PRINTF("get %s stat error.", t_path.c_str());
            ret = false;
            break;
          }

          if (S_ISDIR(buf.st_mode)) {
            dir_list.push_back(t_path);
          } else if (S_ISREG(buf.st_mode)){
            len = strlen(direntp->d_name);
            if (len > 4 && !strcmp(&(direntp->d_name[len - 4]), ".acs")) {
              if (buf.st_size < 10) {
                ret = false;
                break;
              }
            }
          }
        }
      }
      closedir(dirp);
      if (!ret) break;
    }

  } while (0);

  dir_list.clear();

  return ret;
}

bool AMAppsLauncher::init_prepare()
{
  bool ret = true;
  bool mount_f = false;

  do {
    if (check_partition_exist(PARTITION_NAME)) {
      if (!access(FIRST_RUN, F_OK)) {
        if (!check_config(ORYX_PATH)) {
          PRINTF("Oryx configures are incomplete, "
              "begin to restore oryx configures to original...\n");
          mount_f = mount_partition(PARTITION_NAME, MOUNT_DIR,
                                    DEFAULT_MOUNT_FS_TYPE);
          if (mount_f) {
            if (!restore_config(ORYX_BASE, MOUNT_DIR,"/etc")) {
              ret = false;
              break;
            }
            PRINTF("Restore to original Done.\n");
          } else {
            ERROR("Failed to mount %s partition", PARTITION_NAME);
            ret = false;
            break;
          }
        }
      } else {
        PRINTF("First run, begin to backup oryx configures...\n");
        mount_f = mount_partition(PARTITION_NAME, MOUNT_DIR,
                                  DEFAULT_MOUNT_FS_TYPE);
        if (!touch_file(FIRST_RUN)) {
          ret = false;
          break;
        }

        if (mount_f) {
          if (!backup_config(ORYX_BASE, ORYX_PATH, ORYX_PATH, MOUNT_DIR)) {
            ret = false;
            break;
          }
          PRINTF("Backup Done.\n");
        }
      }
    }
  } while (0);

  if (mount_f) {
    umount_dir(MOUNT_DIR);
  }

  return ret;
}

int AMAppsLauncher::init()
{
  int i = 0;
  int ret = 0;
  char oryx_conf_file[] = DEFAULT_APPS_CONF_FILE;
  AMConfig *config = NULL;
  std::string tmp;
  int service_type = -1;
  bool enable_flag = false;
  AMServiceBase *service = NULL;
  char filename[512] = {0};

  do {
    init_prepare();
    //use Config parser to parse acs file to get the service config to load
    m_service_manager = AMServiceManager::get_instance();
    if(!m_service_manager) {
      ERROR("AppsLauncher : unable to get service manager \n");
      ret = -1;
      break;
    }

    m_api_proxy = AMAPIProxy::get_instance();
    if(!m_api_proxy) {
      ERROR("AppsLauncher : unable to get api Proxy \n");
      ret = -2;
      break;
    }

    tmp = std::string(oryx_conf_file);

    config = AMConfig::create(tmp.c_str());
    if (!config) {
      ERROR("AppsLaucher: fail to create from config file\n");
      ret = -3;
      break;
    }

    AMConfig& apps = *config;

    for (i = 0; i < APP_LAUNCHER_MAX_SERVICE_NUM; i ++) {
      if (apps[i]["filename"].exists()) {
        tmp = apps[i]["filename"].get<std::string>("");
        strncpy(m_service_list[i].name,
                tmp.c_str(),
                AM_SERVICE_NAME_MAX_LENGTH);
        m_service_list[i].name[AM_SERVICE_NAME_MAX_LENGTH - 1] = '\0';
      }
      if (apps[i]["type"].exists()) {
        service_type = apps[i]["type"].get<int>(0);
        m_service_list[i].type = AM_SERVICE_CMD_TYPE(service_type);
      }
      if (apps[i]["enable"].exists()) {
        enable_flag = apps[i]["enable"].get<bool>(false);
        m_service_list[i].enable = enable_flag;
      }
      if (apps[i]["event_handle_button"].exists()) {
        enable_flag = apps[i]["event_handle_button"].get<bool>(false);
        m_service_list[i].handle_event_button = enable_flag;
      }
      if (apps[i]["event_handle_motion"].exists()) {
        enable_flag = apps[i]["event_handle_motion"].get<bool>(false);
        m_service_list[i].handle_event_motion = enable_flag;
      }
      if (apps[i]["event_handle_audio"].exists()) {
        enable_flag = apps[i]["event_handle_audio"].get<bool>(false);
        m_service_list[i].handle_event_audio = enable_flag;
      }

      //if a service is enabled, but some fields are invalid, report error
      if (m_service_list[i].enable) {
        if (strlen(m_service_list[i].name) == 0){
          ERROR("AppsLaucher: service %d with empty name \n", i);
          ret = -4;
          break;
        }

        if (m_service_list[i].type == 0) {
          ERROR("AppsLaucher: service %s with type not set, "
              "please set to be \"enum AM_SERVICE_CMD_TYPE\"\n",
              m_service_list[i].name);
          ret = -5;
          break;
        }
      }
    }

    //now add services
    for (i = 0; i < APP_LAUNCHER_MAX_SERVICE_NUM; i ++) {
      if (m_service_list[i].enable) {
        sprintf(filename, "/usr/bin/%s", m_service_list[i].name);
        service = new AMServiceBase(m_service_list[i].name,
                                    filename,
                                    m_service_list[i].type);
        if (!service) {
          ERROR("AppsLaucher: unable to create AMServiceBase object \n");
          ret = -6;
          break;
        }
        m_service_manager->add_service(service);
      }
    }

    if (m_service_manager->init_services() < 0) {
      ERROR("AppsLaucher: server manager init service failed \n");
      ret = -7;
      break;
    }


    INFO("api proxy tries to init AIR API interface\n");
    if(m_api_proxy->init() < 0) {
      ERROR("AppsLaucher: api proxy init failed \n");
      ret = -8;
      break;
    }
    if (m_api_proxy->register_notify_cb(AM_SERVICE_TYPE_EVENT) < 0) {
      WARN("AppsLaucher: register notify callback on event_svc failed \n");
      //no need to return error if register notify callback failed
    }


  }while(0);

  //if incorrect, delete all objects
  if (ret !=0) {
    delete m_api_proxy;
    m_api_proxy = NULL;
  }
  return ret;
}

int AMAppsLauncher::start()
{

  int ret = 0;
  do {
    if (!m_service_manager) {
      ret = -1;
      break;
    }
    //start all of the services
    if (m_service_manager->start_services() < 0) {
      ERROR("AppsLauncher: server manager start service failed \n");
      ret = -2;
      break;
    }
  } while (0);

  return ret;
}


int AMAppsLauncher::stop()
{
  int ret = 0;
  do {
    if (!m_service_manager) {
      ret = -1;
      break;
    }
    if (m_service_manager->stop_services() < 0) {
      ERROR("service manager is unable to stop services\n");
    }
  } while (0);
  return ret;
}

int AMAppsLauncher::clean()
{
  int ret = 0;
  do {
    if (!m_service_manager) {
      ret = -1;
      break;
    }
    PRINTF("clean services.");
    if (m_service_manager->clean_services() < 0) {
      ERROR("service manager is unable to clean services\n");
    }
  } while (0);
  delete m_api_proxy;
  m_api_proxy = NULL;
  m_service_manager = NULL;

  return ret;
}


int main(int argc, char *argv[])
{
  AMAppsLauncher *apps_launcher;
  int ret = 0;
  int wdg_enable = 0;
  signal(SIGINT, sigstop);
  signal(SIGQUIT, sigstop);
  signal(SIGTERM, sigstop);

  if ((argc == 2) && (strcmp(argv[1], "-w") == 0)) {
    wdg_enable = 1;
  } else {
    PRINTF("Watchdog function is not enable. "
        "Please type 'apps_launcher -w' to enable watchdog function!");
  }

  AMPIDLock lock;
  if (lock.try_lock() < 0) {
    ERROR("unable to lock PID, same name process should be running already\n");
    return -1;
  }

  do {
    apps_launcher = AMAppsLauncher::get_instance();
    if (!apps_launcher) {
      ERROR("AppsLauncher: AMAppsLauncher created failed \n");
      ret = -1;
      break;
    }

    //do init
    if (apps_launcher->init() < 0) {
      ERROR("AppsLauncher: init failed \n");
      ret = -2;
      break;
    }
    //start running
    if (apps_launcher->start() < 0) {
      ERROR("AppsLauncher: start failed \n");
      ret = -2;
      break;
    }

    //start watchdog
    if (wdg_enable == 1) {
      gWdInstance = new AMWatchdogService();
      if (AM_LIKELY(gWdInstance &&
                    gWdInstance->init(apps_launcher->m_service_list))) {
        gWdInstance->start();
        PRINTF(" 'apps_launcher -w' enabled watchdog function!");
      } else if (AM_LIKELY(!gWdInstance)) {
        ERROR("Failed to create watchdog service instance!");
        ret =-3;
        break;
      }
    }
  } while (0);

  //handling errors
  if (ret < 0) {
    ERROR("apps launcher met error, clean all services and quit \n");
    goto exit1;
  }

  //////////////////// Handler for Ctrl+C Exit ///////////////////////////////
  PRINTF("press Ctrl+C to quit\n");
  pause();
  exit1:
  if (apps_launcher)  {
    apps_launcher->stop();
    apps_launcher->clean();
    delete apps_launcher;
    if (wdg_enable == 1) {
      delete gWdInstance;
    }
  }
  return ret;
}


