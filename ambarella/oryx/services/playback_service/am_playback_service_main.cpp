/*******************************************************************************
 * am_playback_service_main.cpp
 *
 * History:
 *   2016-04-14 - [Zhi He] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#include "am_base_include.h"
#include "am_define.h"
#include "am_log.h"
#include "am_playback_service_msg_map.h"
#include "am_pid_lock.h"
#include "am_service_frame_if.h"
#include "am_api_playback.h"
#include <signal.h>
enum
{
  IPC_API_PROXY = 0,
  IPC_COUNT
};

AM_SERVICE_STATE  g_service_state  = AM_SERVICE_STATE_NOT_INIT;
AMIServiceFrame  *g_service_frame  = nullptr;
AMIPCBase        *g_ipc_base_obj[] = {nullptr};

static void sigstop(int arg)
{
  INFO("playback_service got signal\n");
}

static int create_control_ipc()
{
  DEBUG("Playback service create_control_ipc\n");
  AMIPCSyncCmdServer *ipc = new AMIPCSyncCmdServer();
  if (ipc && ipc->create(AM_IPC_PLAYBACK_NAME)  < 0) {
    ERROR("receiver create failed \n");
    delete ipc;
    return -1;
  } else {
    g_ipc_base_obj[IPC_API_PROXY] = ipc;
  }

  ipc->REGISTER_MSG_MAP(API_PROXY_TO_PLAYBACK_SERVICE);
  ipc->complete();
  DEBUG("IPC create done for API_PROXY TO PLAYBACK_SERVICE, name is %s \n",
        AM_IPC_PLAYBACK_NAME);
  return 0;
}

int clean_up()
{
  for (uint32_t i = 0; i < sizeof(g_ipc_base_obj) / sizeof(g_ipc_base_obj[0]);
      i ++) {
    delete g_ipc_base_obj[i];
  }
  return 0;
}

int main(int argc, char *argv[])
{
  int ret = 0;
  g_service_frame = AMIServiceFrame::create("Playback.Service");

  if (AM_LIKELY(g_service_frame)) {
    signal(SIGINT,  sigstop);
    signal(SIGQUIT, sigstop);
    signal(SIGTERM, sigstop);

    do {
      AMPIDLock lock;
      if (AM_LIKELY(lock.try_lock() < 0)) {
        ERROR("Unable to lock PID, Playback service is already running!");
        ret = -1;
      } else {
        g_service_state = AM_SERVICE_STATE_INIT_PROCESS_CREATED;
        create_control_ipc();
        g_service_state = AM_SERVICE_STATE_INIT_DONE;
        NOTICE("Entering Playback service main loop!");
        g_service_frame->run();
        NOTICE("Exit Playback service main loop!");
        clean_up();
      }
    } while (0);

    g_service_frame->destroy();
    g_service_frame = nullptr;
    PRINTF("Playback service destroyed!");
  } else {
    ERROR("Failed to create service framework for Playback service!");
    g_service_state = AM_SERVICE_STATE_ERROR;
    ret = -3;
  }

  return ret;
}
