/*******************************************************************************
 * am_muxer_jpeg_base.h
 *
 * History:
 *   2015-10-8 - [ccjing] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#ifndef AM_MUXER_JPEG_H_
#define AM_MUXER_JPEG_H_
#include "am_muxer_codec_if.h"
#include "am_muxer_jpeg_config.h"
#include "am_video_types.h"
#include <deque>
#include <list>
#include <string>

class AMPacket;
class AMJpegFileWriter;
class AMThread;
struct AMMuxerCodecJpegConfig;
class AMMuxerJpegConfig;

#define JPEG_DATA_PKT_ERROR_NUM             50
#define CHECK_FREE_SPACE_FREQUENCY_FOR_JPEG 50

class AMMuxerJpegBase : public AMIMuxerCodec
{
    typedef std::list<std::string> string_list;
  public:
    typedef std::deque<AMPacket*> packet_queue;
    /*interface of AMIMuxerCodec*/
    virtual AM_STATE start();
    virtual AM_STATE stop();
    virtual bool start_file_writing();
    virtual bool stop_file_writing();
    virtual bool is_running();
    virtual AM_MUXER_CODEC_STATE get_state();
    virtual AM_MUXER_ATTR get_muxer_attr() = 0;
    virtual uint8_t get_muxer_codec_stream_id();
    virtual uint32_t get_muxer_id();
    virtual void     feed_data(AMPacket* packet) = 0;
    virtual AM_STATE set_config(AMMuxerCodecConfig *config);
    virtual AM_STATE get_config(AMMuxerCodecConfig *config);
  private :
    virtual void main_loop() = 0;
    virtual AM_STATE generate_file_name(char file_name[]) = 0;
  protected :
    AMMuxerJpegBase();
    virtual ~AMMuxerJpegBase();
    AM_STATE init(const char* config_file);
    static void thread_entry(void *p);
    AM_MUXER_CODEC_STATE create_resource();
    void release_resource();
    bool get_current_time_string(char *time_str, int32_t len);
    bool get_proper_file_location(std::string& file_location);
    void check_storage_free_space();
  protected :
    AM_STATE on_info_packet(AMPacket* packet);
    virtual AM_STATE on_data_packet(AMPacket* packet);
    AM_STATE on_eos_packet(AMPacket* packet);

  protected:
    std::string             m_muxer_name;
    AMThread               *m_thread;
    AMSpinLock             *m_state_lock;
    AMSpinLock             *m_interface_lock;
    AMSpinLock             *m_file_writing_lock;
    AMMuxerCodecJpegConfig *m_muxer_jpeg_config;/*do not need to delete*/
    AMMuxerJpegConfig      *m_config;
    packet_queue           *m_packet_queue;
    char                   *m_config_file;
    AMJpegFileWriter       *m_file_writer;
    std::string             m_file_location;
    AM_MUXER_CODEC_STATE    m_state;
    uint32_t                m_stream_id;
    bool                    m_run;
    bool                    m_file_writing;
};
#endif /* AM_MUXER_JPEG_H_ */
