/**
 * am_video_camera.cpp
 *
 *  History:
 *    Jul 22, 2015 - [Shupeng Ren] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "am_base_include.h"
#include "am_log.h"
#include "am_define.h"
#include "am_video_types.h"
#include "am_encode_types.h"
#include "am_encode_config.h"
#include "am_encode_device.h"
#include "am_video_camera.h"

#define  AUTO_LOCK(mtx)  std::lock_guard<std::recursive_mutex> lck(mtx)

AMVideoCamera *AMVideoCamera::m_instance = nullptr;
std::recursive_mutex AMVideoCamera::m_mtx;

AMIVideoCameraPtr AMIVideoCamera::get_instance()
{
  return AMVideoCamera::get_instance();
}

AMVideoCamera* AMVideoCamera::get_instance()
{
  AUTO_LOCK(m_mtx);
  if (!m_instance) {
    m_instance = AMVideoCamera::create();
  }
  return m_instance;
}

void AMVideoCamera::inc_ref()
{
  ++m_ref_cnt;
}

void AMVideoCamera::release()
{
  AUTO_LOCK(m_mtx);
  if ((m_ref_cnt > 0) && (--m_ref_cnt == 0)) {
    delete m_instance;
    m_instance = nullptr;
  }
}

AMVideoCamera* AMVideoCamera::create()
{
  AMVideoCamera *result = new AMVideoCamera();
  if (result && (AM_RESULT_OK != result->init())) {
    delete result;
    result = nullptr;
  }
  return result;
}

AM_RESULT AMVideoCamera::init()
{
  AM_RESULT result = AM_RESULT_OK;

  do {
    if (!(m_platform = AMIPlatform::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMIPlatform!");
      break;
    }

    if (!(m_device = AMEncodeDevice::create())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMEncodeDevice!");
      break;
    }

    if (!(m_vin_config = AMVinConfig::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMVinConfig!");
      break;
    }

    if (!(m_vout_config = AMVoutConfig::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMVoutConfig!");
      break;
    }

    if (!(m_buffer_config = AMBufferConfig::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMBufferConfig!");
      break;
    }

    if (!(m_stream_config = AMStreamConfig::get_instance())) {
      result = AM_RESULT_ERR_MEM;
      ERROR("Failed to create AMStreamConfig!");
      break;
    }
  } while (0);

  return result;
}

AMVideoCamera::AMVideoCamera() :
    m_ref_cnt(0),
    m_device(nullptr),
    m_platform(nullptr),
    m_vin_config(nullptr),
    m_buffer_config(nullptr),
    m_stream_config(nullptr)
{
  DEBUG("AMVideoCamera is created!");
}

AMVideoCamera::~AMVideoCamera()
{
  AM_DESTROY(m_device);
  m_vin_config = nullptr;
  m_buffer_config = nullptr;
  m_stream_config = nullptr;
  DEBUG("AMVideoCamera is destroyed!");
}

AM_RESULT AMVideoCamera::start()
{
  return m_device->start();
}

AM_RESULT AMVideoCamera::stop()
{
  return m_device->stop();
}

AM_RESULT AMVideoCamera::idle()
{
  return m_device->idle();
}

AM_RESULT AMVideoCamera::preview()
{
  return m_device->preview();
}

AM_RESULT AMVideoCamera::encode()
{
  return m_device->encode();
}

AM_RESULT AMVideoCamera::decode()
{
  return m_device->decode();
}

AM_RESULT AMVideoCamera::get_stream_status(AM_STREAM_ID id,
                                           AM_STREAM_STATE &state)
{
  return m_device->get_stream_status(id, state);
}

uint32_t  AMVideoCamera::get_encode_stream_max_num()
{
  return m_platform->system_max_stream_num_get();
}

uint32_t  AMVideoCamera::get_source_buffer_max_num()
{
  return m_platform->system_max_buffer_num_get();
}

AM_RESULT AMVideoCamera::get_bitrate(AMBitrate &bitrate)
{
  AM_RESULT result = AM_RESULT_OK;
  AMPlatformBitrate pbitrate;
  memset(&pbitrate, 0, sizeof(pbitrate));

  do {
    pbitrate.id = bitrate.stream_id;
    result = m_platform->stream_bitrate_get(pbitrate);
    if (result != AM_RESULT_OK) {
      break;
    }
    bitrate.rate_control_mode = pbitrate.rate_control_mode;
    bitrate.target_bitrate = pbitrate.target_bitrate;
    bitrate.vbr_min_bitrate = pbitrate.vbr_min_bitrate;
    bitrate.vbr_max_bitrate = pbitrate.vbr_max_bitrate;
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::set_bitrate(const AMBitrate &bitrate)
{
  AMPlatformBitrate pbitrate;
  pbitrate.id = bitrate.stream_id;
  pbitrate.rate_control_mode = bitrate.rate_control_mode;
  pbitrate.target_bitrate = bitrate.target_bitrate;
  pbitrate.vbr_min_bitrate = bitrate.vbr_min_bitrate;
  pbitrate.vbr_max_bitrate = bitrate.vbr_max_bitrate;
  return m_platform->stream_bitrate_set(pbitrate);
}

AM_RESULT AMVideoCamera::get_framefactor(AMFrameFactor &factor)
{
  AM_RESULT result = AM_RESULT_OK;
  AMPlatformFramefactor pfactor;

  do {
    pfactor.id = factor.stream_id;
    result = m_platform->stream_framefactor_get(pfactor);
    if (result != AM_RESULT_OK) {
      break;
    }
    factor.fps = pfactor.fps;
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::set_framefactor(const AMFrameFactor &factor)
{
  AMPlatformFramefactor pfactor;
  pfactor.id = factor.stream_id;
  pfactor.fps = factor.fps;
  return m_platform->stream_framefactor_set(pfactor);
}

AM_RESULT AMVideoCamera::get_mjpeg_info(AMMJpegInfo &mjpeg)
{
  AM_RESULT result = AM_RESULT_OK;
  AMPlatformMJPEGConfig pmjpeg;

  do {
    pmjpeg.id = mjpeg.stream_id;
    result = m_platform->stream_mjpeg_quality_get(pmjpeg);
    if (result != AM_RESULT_OK) {
      break;
    }
    mjpeg.quality = pmjpeg.quality;
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::set_mjpeg_info(const AMMJpegInfo &mjpeg)
{
  AMPlatformMJPEGConfig pmjpeg;
  pmjpeg.id = mjpeg.stream_id;
  pmjpeg.quality = mjpeg.quality;
  return m_platform->stream_mjpeg_quality_set(pmjpeg);
}

AM_RESULT AMVideoCamera::get_h264_gop(AMH264GOP &h264)
{
  AM_RESULT result = AM_RESULT_OK;
  AMPlatformH264Config ph264;

  do {
    ph264.id = h264.stream_id;
    result = m_platform->stream_h264_gop_get(ph264);
    if (result != AM_RESULT_OK) {
      break;
    }
    h264.N = ph264.N;
    h264.idr = ph264.idr_interval;
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::set_h264_gop(const AMH264GOP &h264)
{
  AMPlatformH264Config ph264;
  ph264.id = h264.stream_id;
  ph264.N = h264.N;
  ph264.idr_interval = h264.idr;
  return m_platform->stream_h264_gop_set(ph264);
}

AM_RESULT AMVideoCamera::get_stream_offset(AM_STREAM_ID id, AMOffset &offset)
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    result = m_platform->stream_offset_get(id, offset);
    if (result != AM_RESULT_OK) {
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::set_stream_offset(AM_STREAM_ID id, const AMOffset &offset)
{
  return m_platform->stream_offset_set(id, offset);
}

AM_RESULT AMVideoCamera::stop_vin()
{
  return m_device->idle();
}

AM_RESULT AMVideoCamera::halt_vout(AM_VOUT_ID id)
{
  return m_device->halt_vout(id);
}

AM_RESULT AMVideoCamera::force_idr(AM_STREAM_ID stream_id)
{
  return m_device->force_idr(stream_id);
}

AM_RESULT AMVideoCamera::load_config_all()
{
  AM_RESULT result = AM_RESULT_OK;
  do {
    if ((result = m_device->load_config_all()) != AM_RESULT_OK) {
      break;
    }
  } while (0);
  return result;
}

AM_RESULT AMVideoCamera::get_feature_config(AMFeatureParam &config)
{
  return m_platform->feature_config_get(config);
}

AM_RESULT AMVideoCamera::set_feature_config(const AMFeatureParam &config)
{
  return m_platform->feature_config_set(config);
}

AM_RESULT AMVideoCamera::get_vin_config(AMVinParamMap &config)
{
  return m_vin_config->get_config(config);
}

AM_RESULT AMVideoCamera::set_vin_config(const AMVinParamMap &config)
{
  return m_vin_config->set_config(config);
}

AM_RESULT AMVideoCamera::get_vout_config(AMVoutParamMap &config)
{
  return m_vout_config->get_config(config);
}

AM_RESULT AMVideoCamera::set_vout_config(const AMVoutParamMap &config)
{
  return m_vout_config->set_config(config);
}

AM_RESULT AMVideoCamera::get_buffer_config(AMBufferParamMap &config)
{
  return m_buffer_config->get_config(config);
}

AM_RESULT AMVideoCamera::set_buffer_config(const AMBufferParamMap &config)
{
  return m_buffer_config->set_config(config);
}

AM_RESULT AMVideoCamera::get_stream_config(AMStreamParamMap &config)
{
  return m_stream_config->get_config(config);
}

AM_RESULT AMVideoCamera::set_stream_config(const AMStreamParamMap &config)
{
  return m_stream_config->set_config(config);
}

void* AMVideoCamera::get_video_plugin(const std::string& plugin_name)
{
  return m_device->get_video_plugin_interface(plugin_name);
}
